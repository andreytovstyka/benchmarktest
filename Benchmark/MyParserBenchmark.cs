﻿using System;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Running;
using benchmarklesson;

namespace Benchmark
{
    [MemoryDiagnoser]
    [RankColumn]
    public class MyParserBenchmark
    {
        private const string STRING_TO_PARSE_WITH_ERROR = "sdf45";
        private const string STRING_TO_PARSE = "55";

        private readonly MyParser _myParser = new MyParser();

        [Benchmark]
        public void TryCatchParseTestWithError()
        {
            int result = _myParser.TryCatchParse(STRING_TO_PARSE_WITH_ERROR);
        }

        [Benchmark]
        public void TryParseTestWithError()
        {
            int result = _myParser.TryParse(STRING_TO_PARSE_WITH_ERROR);
        }

        [Benchmark]
        public void TryParseFixedTestWithError()
        {
            int result = _myParser.TryParseFixed(STRING_TO_PARSE_WITH_ERROR);
        }

        [Benchmark]
        public void TryCatchParseTest()
        {
            int result = _myParser.TryCatchParse(STRING_TO_PARSE);
        }

        [Benchmark]
        public void TryParseTest()
        {
            int result = _myParser.TryParse(STRING_TO_PARSE);
        }

        [Benchmark]
        public void TryParseFixedTest()
        {
            int result = _myParser.TryParseFixed(STRING_TO_PARSE);
        }

    }
}
